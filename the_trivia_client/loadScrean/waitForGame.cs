﻿using System;
using System.Net.Sockets;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class waitForGame : Form
    {
        private bool _isAdmin;
        private string _roomName;
        private string _maxNumOfPlayers;
        private string _numOfQuestions;
        private string _timePerQuestion;
        private string _username;

        private NetworkStream _clientStream;
        private byte[] bufferGet;
        private byte[] bufferSet;


        public waitForGame(NetworkStream clientStream, string username, bool isAdmin, 
                           string roomName, string maxNumOfPlayers, string numOfQuestions, string timePerQuestion)
        {
            _clientStream = clientStream;
            _username = username;
            _isAdmin = isAdmin;
            _roomName = roomName;
            _maxNumOfPlayers = maxNumOfPlayers;
            _numOfQuestions = numOfQuestions;
            _timePerQuestion = timePerQuestion;

            InitializeComponent();


            userNameLable.Text = _username;
            roomNameLabel.Text = _roomName;

            connactionInfoLabel.Text = "You are connected to the room: " + _roomName;
        }

        private void waitForGame_Load(object sender, EventArgs e)
        {
            if (_isAdmin == true) // the user is the admin
            {

            }
            else // _isAdmin == false (the user is not the admin) 
            {

            }
        }
    }
}
