﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class myStatus : Form
    {
        private NetworkStream _clientStream;
        private byte[] bufferGet;
        private byte[] bufferSet;

        private string _username = "";

        public myStatus(NetworkStream clientStream, string username)
        {
            _clientStream = clientStream;
            _username = username;

            InitializeComponent();
        }

        private void myStatus_Load(object sender, EventArgs e)
        {
            string numberOfGames = "";
            string numberOfRightAns = "";
            string numberOfWrongAns = "";
            string avgTimeForAns = "";
            string msgCode = "";

            UserLabel.Text = "Great job " + _username + "," + " here is your personal status:";

            // sending the message code (225) to the server
            bufferSet = new ASCIIEncoding().GetBytes("225");
            _clientStream.Write(bufferSet, 0, bufferSet.Length);
            _clientStream.Flush();

            bufferGet = new byte[3];
            int bytesRead1 = _clientStream.Read(bufferGet, 0, 3);
            msgCode = new ASCIIEncoding().GetString(bufferGet);

            bufferGet = new byte[4];
            int bytesRead2 = _clientStream.Read(bufferGet, 0, 4);
            numberOfGames = new ASCIIEncoding().GetString(bufferGet);

            bufferGet = new byte[6];
            int bytesRead3 = _clientStream.Read(bufferGet, 0, 6);
            numberOfRightAns = new ASCIIEncoding().GetString(bufferGet);

            bufferGet = new byte[6];
            int bytesRead4 = _clientStream.Read(bufferGet, 0, 6);
            numberOfWrongAns = new ASCIIEncoding().GetString(bufferGet);

            bufferGet = new byte[4];
            int bytesRead5 = _clientStream.Read(bufferGet, 0, 4);
            avgTimeForAns = new ASCIIEncoding().GetString(bufferGet);


            NumOfGamesLabel.Text = "Number of games - " + Convert.ToInt32(numberOfGames);
            NumOfRightAnsLabel.Text = "Number of right answers - " + Convert.ToInt32(numberOfRightAns);
            NumOfWrongAnsLabel.Text = "Number of wrong answers - " + Convert.ToInt32(numberOfWrongAns);

            if(avgTimeForAns[0] == '0')
            {
                avgTimeForAns = avgTimeForAns.Substring(1); // cuting the '0' from the string
                avgTimeForAns = avgTimeForAns[0] + "." + avgTimeForAns.Substring(1, 2);
            }
            else
            {
                avgTimeForAns = avgTimeForAns.Substring(0, 2) + "." + avgTimeForAns.Substring(2, 2);
            }

            AvgTimeForAnsLabel.Text = "Average time for answer - " + avgTimeForAns;
        }

        private void GoBackButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }
    }
}
